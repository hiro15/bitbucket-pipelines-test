package main

import (
	"github.com/go-redis/redis"
)

func SetData() error {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	return client.Set("test_key", "test_value", 0).Err()
}

func main() {
}
